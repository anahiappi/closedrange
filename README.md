# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

JaSST'18 Tokyoの@twadaのTDDチュートリアル

### How do I get set up? ###

* Eclipse(Pleiades all in one) 4.7くらいで動くと思います。
* 同梱のJDK 9
* 同梱のJUnit 5

# 演習
整数閉区間を示すクラスを作りたい。
https://gist.github.com/twada/df6bf4b2d1828919f778cac705cd2ea8

## 問題文
整数閉区間を示すクラス（あるいは構造体）をつくりたい。 整数閉区間は下端点と上端点を持ち、文字列としても表現できる（例: 下端点 3, 上端点 8 の整数閉区間の文字列表記は `"[3,8]"`）。 整数の閉区間は指定した整数を含むかどうかを判定できる。

## 私が作ったToDoリスト
- [x] 整数閉区間を示すクラス
    - [x] 整数閉区間は下端点と上端点を持つ
        - [x] 下端3,上端8でインスタンス生成し、下端3が取得できること
        - [x] 下端4,上端8でインスタンス生成し、下端4が取得できること
        - [x] 下端3,上端8でインスタンス生成し、上端8が取得できること
- [x] 下端点と上端点を文字列にして返す
    - [x] 下端3上端8のインスタンスに対し文字列[3，8]を返す
    - [x] 下端4上端8のインスタンスに対し文字列[4，8]を返す
- [x] 整数の閉区間は指定した整数を含むかどうかを判定できる。
    - [x] [3,8]において3は含むと判断する
    - [x] [3,8]において2は含まないと判断する
- [ ] 下端>=上端の時のエラー処理

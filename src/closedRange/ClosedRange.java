package closedRange;

public class ClosedRange {
	private int lowerEndpoint;
	private int upperEndpoint;

	public ClosedRange(int lowerEndpoint, int upperEndpoint) {
		this.lowerEndpoint = lowerEndpoint;
		this.upperEndpoint = upperEndpoint;
	}

	public int getLowerEndpoint() {
		return lowerEndpoint;
	}

	public int getUpperEndpoint() {
		return upperEndpoint;
	}

	@Override
	public String toString() {
		return "[" + lowerEndpoint + "," + upperEndpoint + "]";
	}

	public boolean contains(int value) {
		if (value >= lowerEndpoint && value <= upperEndpoint) {
			return true;
		}
		return false;
	}

}

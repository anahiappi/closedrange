package closedRange;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

class ClosedRangeTest {

	@Nested
	class 整数閉区間を示すクラス {
		@Nested
		class 整数閉区間は下端点と上端点を持つ {
			@Test
			void 下端3上端8でインスタンス生成し下端3が取得できること() {
				int actual = new ClosedRange(3, 8).getLowerEndpoint();
				assertEquals(3, actual);
			}

			@Test
			void 下端4上端8でインスタンス生成し下端4が取得できること() {
				int actual = new ClosedRange(4, 8).getLowerEndpoint();
				assertEquals(4, actual);
			}

			@Test
			void 下端3上端8でインスタンス生成し上端8が取得できること() {
				int actual = new ClosedRange(3, 8).getUpperEndpoint();
				assertEquals(8, actual);
			}
		}
	}

	@Nested
	class 下端点と上端点を文字列にして返す {
		@Test
		void 下端3上端8のインスタンスに対し文字列で返す() {
			ClosedRange actual = new ClosedRange(3, 8);
			assertEquals("[3,8]", actual.toString());
		}

		@Test
		void 下端4上端8のインスタンスに対し文字列で返す() {
			ClosedRange actual = new ClosedRange(4, 8);
			assertEquals("[4,8]", actual.toString());
		}
	}

	@Nested
	class 整数の閉区間は指定した整数を含むかどうかを判定できる {

		@Test
		void 下端3上端8において3は含むと判断する() {
			ClosedRange actual = new ClosedRange(3, 8);
			assertEquals(true, actual.contains(3));
		}

		@Test
		void 下端3上端8において2は含まないと判断する() {
			ClosedRange actual = new ClosedRange(3, 8);
			assertEquals(false, actual.contains(2));
		}
	}

}
